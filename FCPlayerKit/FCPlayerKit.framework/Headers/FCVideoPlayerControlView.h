//
//  FCVideoPlayerControlView.h
//  VideoTV001
//
//  Created by ZhouYou on 2019/8/13.
//  Copyright © 2019 DengJie. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FCVideoPlayerControlView;
@protocol FCVideoPlayerControlViewDelegate <NSObject>

@optional
- (void)controlViewPlayBtnClicked:(FCVideoPlayerControlView *)controlView;
- (void)controlViewFullScreenClicked:(FCVideoPlayerControlView *)controlView;
// 滑动结束
- (void)controlViewSlicerChanged:(FCVideoPlayerControlView *)controlView;
// 滑动中
- (void)controlViewSlicerDragInside:(FCVideoPlayerControlView *)controlView;
@end
 
@interface FCVideoPlayerControlView : UIView

- (id)initWithFrame:(CGRect)frame type:(NSInteger) type;

@property (nonatomic, strong) UILabel *durationLabel;
@property (nonatomic, strong) UILabel *playTimeLabel;
@property (nonatomic, strong) UIButton *bottomPlayBtn;
@property (nonatomic, strong) UIButton *fullScreenBtn;
@property (nonatomic, strong) UIProgressView *bufferingView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *centerText;
@property (nonatomic, assign) id<FCVideoPlayerControlViewDelegate> delegate;

@property (nonatomic, assign) BOOL isLive;
/** 0：视频类型 1： 音频类型布局 */
@property (nonatomic, assign) NSInteger type;
@property(nonatomic) float value;                         

@end

NS_ASSUME_NONNULL_END
