//
//  FCVideoRollingView.h
//  FCPlayerKit
//
//  Created by mac on 2020/9/27.
//  Copyright © 2020 DengJie. All rights reserved.
//

#import <PLPlayerKit/PLPlayerKit.h>
#import <UIKit/UIKit.h>
#define kVideoRollingViewManager [FCVideoRollingViewManager sharedInstance]
NS_ASSUME_NONNULL_BEGIN

@interface FCVideoRollingView : UIView

 
@property (nonatomic, strong) PLPlayer *player;

@property (nonatomic, copy) NSString *playUrl;

@property (nonatomic, copy) NSString *preUrl;

- (void)preloadingPlayUrl:(NSString *) url preUrl:(NSString *)preUrl;
@end

@interface FCVideoRollingViewManager : NSObject

@property (nonatomic, assign) NSInteger video_id;
/** 初始化 */
+ (instancetype)sharedInstance;

- (void)show;

- (void)hidden;
///  暂停后继续
- (void)resume;
///  暂停 
- (void)pause;

//- (void)destruction;
///  暂停
- (void)stop;

- (void)seekTo:(CMTime)time;
@property (nonatomic, strong) FCVideoRollingView *coView;
///  隐藏之前的播放时间
@property (nonatomic, assign) CMTime  currentTime;

@property (nonatomic, copy) NSString *playUrl;

@end

NS_ASSUME_NONNULL_END
