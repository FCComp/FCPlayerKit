//
//  FCAudioPlayerItem.h
//  FCPlayerKit
//
//  Created by ZhouYou on 2020/5/15.
//  Copyright © 2020 DengJie. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCAudioPlayerItem : NSObject

@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *albumTitle;
@property (nonatomic, copy) NSString *singer;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic ,assign) NSInteger Id;
@property (nonatomic, strong) NSDictionary *otherInfo;
@property (nonatomic, copy) NSString *tf;

@end

NS_ASSUME_NONNULL_END
