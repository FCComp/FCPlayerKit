//
//  FCAudioPlayerManager.h
//  FCPlayerKit
//
//  Created by ZhouYou on 2020/5/15.
//  Copyright © 2020 DengJie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <FCPlayerKit/FCAudioPlayerItem.h>
#import <UIKit/UIKit.h>
#import "FCAudioPlayerRollingView.h"
#import <PLPlayerKit/PLPlayerKit.h>
#import <qplayer2_core/qplayer2_core.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    FCAudioPlayerState_unkown,
    FCAudioPlayerState_playing,
    FCAudioPlayerState_pause,
    FCAudioPlayerState_stop,
} FCAudioPlayerState;

typedef enum : NSUInteger {
    FCAudioPlayerAction_Play,
    FCAudioPlayerAction_Pause,
    FCAudioPlayerAction_Stop,
    FCAudioPlayerAction_Resume,
    FCAudioPlayerAction_PreviousPlay,
    FCAudioPlayerAction_NextPlay,
    FCAudioPlayerAction_SeekComplete,
    FCAudioPlayerAction_Ready,// 读取中
    FCAudioPlayerAction_Failed,
} FCAudioPlayerAction;

@interface FCAudioPlayerManager : NSObject

@property (nonatomic, strong) FCAudioPlayerRollingBGView *coView;
@property (nonatomic, strong, nullable) PLPlayer *plPlayer;
@property (nonatomic, strong, nullable) QPlayerContext *mPlayer;
 
@property (nonatomic, strong, nullable) NSArray<FCAudioPlayerItem *> *musicItems;
@property (nonatomic, assign) NSInteger playIndex;
 
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) FCAudioPlayerState playerState;
@property (nonatomic, assign) BOOL completeRePlay;

@property (nonatomic, strong) UIViewController *playViewController;
@property (nonatomic, copy) void(^fcAudioPlayerActionCallback)(FCAudioPlayerState playerState, FCAudioPlayerItem *playItem, NSInteger playIndex, FCAudioPlayerAction playerAction);
@property (nonatomic, copy,nullable) void(^fcAudioPlayerDurationCallback)(FCAudioPlayerItem *playItem, CGFloat duration, CGFloat currentTime);

+ (FCAudioPlayerManager *)instance;
- (CGFloat)getCurrentTime;
- (CGFloat)getDuration;

- (void)play;
- (void)pause;
///继续播放
- (void)resume;
- (void)stop;

- (void)previousPlay;
- (void)nextPlay;
- (void)seekToSecend:(CGFloat)sec;

- (void)showRolling;
- (void)hiddenRolling;
- (void)setAllButtonUserInteractionEnabled:(BOOL) userInteractionEnabled;
@end

NS_ASSUME_NONNULL_END
