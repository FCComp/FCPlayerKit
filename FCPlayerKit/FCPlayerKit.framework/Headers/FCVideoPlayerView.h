//
//  FCVideoPlayerView.h
//  VideoTV001
//
//  Created by ZhouYou on 2019/8/13.
//  Copyright © 2019 DengJie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PLPlayerKit/PLPlayerKit.h>
#import "FCVideoPlayerControlView.h"
#import <qplayer2_core/qplayer2_core.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCVideoPlayerView : UIView

@property (strong, nonatomic) FCVideoPlayerControlView *controlView;

@property (nonatomic, strong) PLPlayer *player; 

@property (nonatomic, strong, nullable) QPlayerContext *mPlayer;
@property (nonatomic, strong) UIView * playerView;

 
@property (nonatomic, copy) NSString *referer;
/*
 视频播放地址，必须
 */
@property (nonatomic, copy) NSString *playUrl;

/*
 视频播放前预览图片地址，可选
 */
@property (nonatomic, copy) NSString *preUrl;

/*
 视频播放前预览图片默认图，可选
 */
@property (nonatomic, strong) UIImage *placeholderImage;

/*
 是否直播，默认NO
 */
@property (nonatomic, assign) BOOL isLive;

/*
 循环播放，默认NO
 */
@property (nonatomic, assign) BOOL loopPlay;

/*
 是否显示控制按钮，默认NO
 */
@property (nonatomic, assign) BOOL needControlView;
/*
 是否隐藏 volumeView  默认显示
 */
@property (nonatomic, assign) BOOL isVolumeViewHidden;

 
/*
 是否已停止播放
 */
@property (nonatomic, assign, readonly) BOOL isStop;

/*
 中间播放按钮图标，可选
 */
@property (nonatomic, strong) UIImage *playBtnImage;

/*
 中间播放按钮播放中图标，可选
 */
@property (nonatomic, strong) UIImage *playBtnSelectImage;

/*
 视频开始播放 回调
 */
@property (nonatomic, copy) void(^playerWillPlay)(FCVideoPlayerView *);

/*
 点击全屏按钮，回调，全屏功能自行开发
 */
@property (nonatomic, copy) void(^playerEnterFullscreen)(FCVideoPlayerView *);

/*
 点击退出全屏按钮，回调，退出全屏功能自行开发
 */
@property (nonatomic, copy) void(^playerExitFullscreen)(FCVideoPlayerView *);

/*
 播放界面控制器隐藏与开启回调
 */
@property (nonatomic, copy) void(^playerControlBarShow)(BOOL);
/*
 播放地址为空的时候调用
 */
@property (nonatomic, copy) void(^playerNill)(void);

/*
 播放
 */
- (void)play;

/*
 停止
 */
- (void)stop;

/*
 暂停
 */
- (void)pause;

/*
 seak到其他位置播放
 */
- (void)seakTo:(float)seak;
@end

@interface FCVideoPlayerCompleteView : FCVideoPlayerView

@end
NS_ASSUME_NONNULL_END
