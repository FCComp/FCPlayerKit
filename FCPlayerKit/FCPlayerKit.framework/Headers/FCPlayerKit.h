//
//  FCPlayerKit.h
//  FCPlayerKit
//
//  Created by ZhouYou on 2019/8/15.
//  Copyright © 2019 DengJie. All rights reserved.
// 0.0.10

#import <UIKit/UIKit.h>

//! Project version number for FCPlayerKit.
FOUNDATION_EXPORT double FCPlayerKitVersionNumber;

//! Project version string for FCPlayerKit.
FOUNDATION_EXPORT const unsigned char FCPlayerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FCPlayerKit/PublicHeader.h>

#import <FCPlayerKit/FCVideoPlayerView.h>
#import <FCPlayerKit/FCAudioPlayerManager.h>
#import <FCPlayerKit/FCAudioPlayerItem.h>
#import <FCPlayerKit/FCVideoFullScreenVc.h>
#import <FCPlayerKit/FCVideoRollingView.h>

 
 
