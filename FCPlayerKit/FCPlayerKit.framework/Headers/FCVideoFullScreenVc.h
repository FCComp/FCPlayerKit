//
//  LiveI003FullScreenVc.h
//  LiveI003
//
//  Created by omni－appple on 2018/12/21.
//  Copyright © 2018年 ZhouYou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FCBaseKit/FCBaseKit.h>

@interface FCVideoFullScreenVc : FCViewController

@property (nonatomic,copy)void (^callbackScreenUp)(UIDeviceOrientation Or);
 
@end

@interface FCVideoFull2ScreenVc : UIViewController

@property (nonatomic,copy)void (^callbackScreenUp)(UIDeviceOrientation Or);

@end
@interface FCVideoFull3ScreenVc : FCVideoFullScreenVc
/// 是否屏幕 左边进入
@property (nonatomic, assign) BOOL  isLeft;

@end
