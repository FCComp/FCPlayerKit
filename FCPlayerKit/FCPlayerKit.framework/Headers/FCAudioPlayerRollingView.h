//
//  FCAudioPlayerRollingView.h
//  FCPlayerKit
//
//  Created by ZhouYou on 2020/5/15.
//  Copyright © 2020 DengJie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FCAudioPlayerRollingBGViewDelegate <NSObject>
 
@optional
- (void)click_last;
- (void)click_stop_play:(UIButton *)but;
- (void)click_next;
- (void)click_exit;
 

@end


@interface FCAudioPlayerRollingView : UIView
@property (nonatomic, weak) UIButton *btn_stop;
@property (nonatomic, strong) UIImageView *img_bgView ;

@property (nonatomic, weak) UIButton *btn_next;

@property (nonatomic, weak) UIButton *btn_last;

@property (nonatomic, weak) UIButton *btn_exit;

+ (FCAudioPlayerRollingView *)rollingWithButtomView:(UIView*)buttomView  size:(CGSize)size;
- (void)drawProgress:(CGFloat)progress;
- (void)takeBack;
@end


@interface FCAudioPlayerRollingBGView : UIView

@property (nonatomic, weak) id<FCAudioPlayerRollingBGViewDelegate> delegate;
@property (nonatomic, strong) FCAudioPlayerRollingView *topView;
@property (nonatomic, strong) UIButton *btn_bg;

- (void)resetImage:(UIImage * _Nullable)image;
- (void)drawProgress:(CGFloat)progress;
- (void)enableNextAndPreBtn:(BOOL)enable;
- (void)playing:(BOOL)playing;
@end
NS_ASSUME_NONNULL_END
